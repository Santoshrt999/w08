(function ($) {

  $('#btnLoadText').click(function () { $("#showResult").load("show.txt").css({"color": "green", "font-family": "Palatino Linotype, Book Antiqua, Palatino, serif", "font-size": "300%"}); });
  $('#btnAjax').click(function () { callRestAPI() });

  // Perform an asynchronous HTTP (Ajax) API request.
  function callRestAPI() {
    var root = 'https://jsonplaceholder.typicode.com';
    $.ajax({
      url: root + '/posts/1',
      method: 'GET'
    }).then(function (response) {
      console.log(response);
      $('#showResult').html(response.body);
    });
  }
})(jQuery);